package com.persist;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Test;

import com.model.Veiculo;
import com.util.JpaUtil;

public class PersistentTest {

	@Test
	public void testeInsert() {
		EntityManager manager = JpaUtil.getEntityManager();
		EntityTransaction tn = manager.getTransaction();

		tn.begin();

		Veiculo veiculo = new Veiculo();
		veiculo.setFabricante("Chevrolet");
		veiculo.setModelo("Celta");
		veiculo.setAnoFabricacao(2015);
		veiculo.setAnoModelo(2016);
		veiculo.setValor(new BigDecimal(31000));

		manager.persist(veiculo);
		tn.commit();
		manager.close();
	}
	
	@Test
	public void testefind() {
		EntityManager manager = JpaUtil.getEntityManager();

		Veiculo veiculo = manager.find(Veiculo.class, 3L);
		Assert.assertEquals("Honda Fit", veiculo.getModelo());
		
		Veiculo veiculo2 = manager.find(Veiculo.class, 3L);
		
		manager.close();
	}

	@Test
	public void testeList() {
		EntityManager manager = JpaUtil.getEntityManager();

		Query findAll = manager.createQuery("FROM Veiculo");
		
		@SuppressWarnings("unchecked")
		List<Veiculo> lista = findAll.getResultList();

		for (Veiculo v : lista) {
			System.out.println(v.toString());
		}

		manager.close();
	}
	
	@Test
	public void testeUpdate(){
		EntityManager manager = JpaUtil.getEntityManager();
		EntityTransaction tn = manager.getTransaction();
		tn.begin();
		
		Veiculo veiculo = manager.find(Veiculo.class, 10L);
		veiculo.setValor(new BigDecimal(26500));
		veiculo.setAnoFabricacao(2011);
		veiculo.setAnoModelo(2011);

		tn.commit();
		
		manager.close();
	}
	
	@Test
	public void testeDelete(){
		EntityManager manager = JpaUtil.getEntityManager();
		EntityTransaction tn = manager.getTransaction();
		tn.begin();
		
		Veiculo veiculo = manager.find(Veiculo.class, 4L);
		if(veiculo != null)
			manager.remove(veiculo);

		tn.commit();
		manager.close();
	}
	
	public void end() {
		JpaUtil.close();
	}

}
